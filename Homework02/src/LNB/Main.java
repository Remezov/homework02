package LNB;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("The number of seconds?");
        int sec = in.nextInt();
        int hour = sec / 3600;
        int min = (sec % 3600) / 60;
        sec = (sec % 3600) % 60;
        System.out.printf("%d hours %d minutes %d seconds", hour, min, sec);
    }
}
