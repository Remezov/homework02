package Salary;

public class Main {

    public static void main(String[] args) {
        double salary = Double.parseDouble(args[0]);
        double tax = Double.parseDouble(args[1]);
        salary -= salary * tax;
        System.out.printf("Sum = %.2f Rub", salary);
    }
}
